package de.wrenchbox.easypath;

import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class CompassListener implements Listener {
	
	HashMap<String, Long> compassCD = new HashMap<String, Long>();
	
	public CompassListener() {
		EasyPath.getPlugin().getServer().getPluginManager().registerEvents(this, EasyPath.getPlugin());
	}
	
	@EventHandler
	public void onUseCompass(PlayerInteractEvent event) {
		if(event.getAction() != Action.RIGHT_CLICK_BLOCK && event.getAction() != Action.RIGHT_CLICK_AIR) {
			return;
		}
		Player player = event.getPlayer();
		
		if(event.getItem() != null && event.getItem().getType() == Material.COMPASS) {
			if(!player.hasPermission("easypath.compass")) {
				player.sendMessage(ChatColor.RED+"You do not have permission to use the pathfinder.");
				return;
			}
			if(compassCD.get(player.getName()) != null && compassCD.get(player.getName())+30*1000 > System.currentTimeMillis()) {
				player.sendMessage(String.format(ChatColor.RED+"You have to wait %.2f seconds to create a new path.", (compassCD.get(player.getName())+30*1000-System.currentTimeMillis())/1000.0));
				return;
			}
			if(!player.getLocation().getBlock().getRelative(BlockFace.DOWN).getType().isSolid()) {
				player.sendMessage(ChatColor.RED+"You have to be on the ground.");
				return;
			}
				
			compassCD.put(player.getName(), System.currentTimeMillis());
			BukkitRunnable patfinder = new Pathfinder(player);
			patfinder.runTaskAsynchronously(EasyPath.getPlugin());
		}
	}
}
