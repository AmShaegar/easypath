package de.wrenchbox.easypath;

import java.io.IOException;

import org.bukkit.ChatColor;
import org.bukkit.block.BlockFace;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class EasyPath extends JavaPlugin {

	private static Plugin plugin;

	@Override
	public void onEnable() {
		plugin = this;
		
		try {
			Metrics metrics = new Metrics(this);
			metrics.start();
		} catch (IOException e) {
			getLogger().info("Unable to connect to metrics server.");
		}
		
		new CompassListener();
	}

	@Override
	public void onDisable() {
		HandlerList.unregisterAll();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command,	String label, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player) sender;
			if(label.equalsIgnoreCase("escape")) {
				if(!player.getLocation().getBlock().getRelative(BlockFace.DOWN).getType().isSolid()) {
					player.sendMessage(ChatColor.RED+"You have to be on the ground.");
					return true;
				}
				BukkitRunnable patfinder = new Pathfinder(player);
				patfinder.runTaskAsynchronously(this);
			}
		}
		return true;
	}
	
	public static Plugin getPlugin() {
		return plugin;
	}
	
}
