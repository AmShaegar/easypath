package de.wrenchbox.easypath;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

public class Pathfinder extends BukkitRunnable {

	private Player player;
	private static Map<String, BukkitTask> tasks = new HashMap<String, BukkitTask>();
	private static Map<String, BukkitRunnable> cleaner = new HashMap<String, BukkitRunnable>();

	public Pathfinder(final Player player) {
		this.player = player;
	}

	@Override
	public void run() {
		if(tasks.containsKey(player.getName())) {
			tasks.get(player.getName()).cancel();
		}
		if(cleaner.containsKey(player.getName())) {
			cleaner.get(player.getName()).runTask(EasyPath.getPlugin());
		}
		if(!searchPath(new Step(player.getLocation().getBlock(), null))) {
			player.sendMessage("No path to surface found. Searching link...");
			if(!searchLink(new Step(player.getLocation().getBlock(), null))) {
				player.sendMessage("No link to surface found.");
			}
		}
	}

	public boolean searchPath(Step previous) {
		final LinkedList<Block> buffer = new LinkedList<Block>();
		final LinkedList<Step> queue = new LinkedList<Step>();
		int steps = 0;
		do {
			if(previous.getBlock().getLightFromSky() >= 15 && previous.getBlock().getY() >= 64) {
				createPath(previous);
				return true;
			}
			if(++steps % 5000 == 0) {
				player.sendMessage("["+(steps/5000)+"] Searching path...");
			}
			for(int i = 0; i < 4; i++) {
				Block relative = previous.getBlock().getRelative(BlockFace.values()[i]).getRelative(BlockFace.UP);
				int height = 0;
				while(!relative.getType().isOccluding() && !relative.getRelative(BlockFace.DOWN).getType().isOccluding() && height <= 3) {
					height++;
					relative = relative.getRelative(BlockFace.DOWN);
				}
				if(!buffer.contains(relative)) {
					if(isWalkable(relative)) {
						buffer.add(relative);
						queue.add(new Step(relative, previous));
					}
				}
			}
		} while((previous = queue.poll()) != null);
		return false;
	}

	private boolean isWalkable(Block b) {
		return !b.getRelative(BlockFace.UP).getType().isOccluding()
				&& !b.getType().isOccluding()
				&& b.getRelative(BlockFace.DOWN).getType().isOccluding();
	}

	public boolean searchLink(Step previous) {
		final LinkedList<Block> buffer = new LinkedList<Block>();
		final LinkedList<Step> queue = new LinkedList<Step>();
		int steps = 0;
		do {
			if(previous.getBlock().getLightFromSky() >= 15 && previous.getBlock().getY() >= 64) {
				createPath(previous);
				return true;
			}
			if(++steps % 5000 == 0) {
				player.sendMessage("["+(steps/5000)+"] Searching link...");
			}
			for(int i = 0; i < 6; i++) {
				Block relative = previous.getBlock().getRelative(BlockFace.values()[i]);
				if(!buffer.contains(relative)) {
					if(isConnected(relative)) {
						buffer.add(relative);
						queue.add(new Step(relative, previous));
					}
				}
			}
		} while((previous = queue.poll()) != null);
		return false;
	}

	private boolean isConnected(Block b) {
		return isConnected(b, true);
	}

	private boolean isConnected(Block b, boolean deep) {
		boolean isConnected = false;
		for(int i = 0; i < 6; i++) {
			Block r = b.getRelative(BlockFace.values()[i]);
			if(r.getType().isOccluding()) {
				isConnected = true;
			} else if(deep) {
				isConnected = isConnected(r, false);
			}
		}
		return !b.getType().isOccluding() && isConnected;
	}

	private void createPath(final Step light) {
		LinkedList<Block> buffer = new LinkedList<Block>();
		Step s = light;
		do {
			buffer.addFirst(s.getBlock());
		} while((s = s.getPrevious()) != null);
		player.sendMessage("Found! Length: "+buffer.size()+" blocks");
		showPath(buffer);
	}

	private void showPath(final LinkedList<Block> buffer) {
		DyeColor color = DyeColor.RED;
		Block p = null;
		double distMinSq = Double.MAX_VALUE;
		for(int i = 0; i < buffer.size(); i++) {
			if(color == DyeColor.RED && i >= buffer.size()/3) {
				color = DyeColor.YELLOW;
			} else if(color == DyeColor.YELLOW && i >= 2*buffer.size()/3) {
				color = DyeColor.GREEN;
			}
			Block b = buffer.get(i);
			while(b.getRelative(BlockFace.DOWN).isEmpty()) {
				b = b.getRelative(BlockFace.DOWN);
			}
			
			double d = player.getLocation().distanceSquared(b.getLocation());
			if(d < distMinSq) {
				distMinSq = d;
			}
			if(d < 2500) { // only show path when in 50 block radius
				if(b.isEmpty() && b.getRelative(BlockFace.DOWN).getType().isOccluding()) {
					player.sendBlockChange(b.getLocation(), Material.CARPET, color.getWoolData());
				}
				if(p != null && b.getY()-p.getY() > 1) {
					Location l = p.getLocation();
					l.setY(b.getY());
					player.sendBlockChange(p.getLocation(), Material.SIGN_POST, (byte) (l.getBlock().getFace(b).ordinal()*4));
				}
			}
			p = b;
		}

		cleaner.put(player.getName(), new BukkitRunnable() {

			@Override
			public void run() {
				cleaner.remove(player.getName());
				for(Block b : buffer) {
					while(b.getRelative(BlockFace.DOWN).isEmpty()) {
						b = b.getRelative(BlockFace.DOWN);
					}
					player.sendBlockChange(b.getLocation(), b.getType(), b.getData());
				}
				player.sendMessage("Path vanished.");
			}
		});

		if(distMinSq > 225) { // clean path if player is 15 blocks away
			if(cleaner.containsKey(player.getName())) {
				cleaner.get(player.getName()).runTask(EasyPath.getPlugin());
			}
		} else {
			tasks.put(player.getName(), new BukkitRunnable() {

				@Override
				public void run() {
					showPath(buffer);
				}
			}.runTaskLaterAsynchronously(EasyPath.getPlugin(), 10*20));
		}
	}
}
