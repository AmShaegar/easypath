package de.wrenchbox.easypath;

import org.bukkit.block.Block;

public class Step {
	
	private Step previous;
	private Block block;
	
	public Step(Block block, Step previous) {
		this.block = block;
		this.previous = previous;
	}
	
	public Block getBlock() {
		return block;
	}
	
	public Step getPrevious() {
		return previous;
	}
}
